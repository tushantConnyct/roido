import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import {Card, CardText} from 'material-ui/Card';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
  titleStyle: {
   color: 'rgb(0, 188, 212)',
 },
};

const dashboardData = [
  {
    id: 1,
    name: 'Number of requests'
  },
  {
    id: 2,
    name: 'Number of user clicks'
  },
  {
    id: 3,
    name: 'Number of leads'
  }
];


class Dashboard extends React.PureComponent {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="row">
          {dashboardData.map((data) => (
            <div className="col s12 col m4">
              <h4>{data.name}</h4>
              <Card key={data.id}>
                <CardText />
              </Card>
            </div>
          ))}
      </div>
    );
  }
}

export default Dashboard;
