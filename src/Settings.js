import React from 'react';
import axios from 'axios';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import './Settings.css';

class Settings extends React.PureComponent {
  constructor(props) {
    super(props);
    // data state is defined to store the data that will be fetched from the api
    this.state = {
      // data: []
      verified: false
    }
  }

// Note: If the app is not too large, by large i mean, if we do not have large data to handle, this will be okay otherwise we
// have to use redux for handling state/store

//TODO - Fetch user data from the server
  // componentDidMount(){
  //   this.loadUserDataFromServer();
  // }
  //
  // loadUserDataFromServer() {
  //   axios.get('http://restapiurlhere')
  //    .then((response) => {
  //       this.setState({ data: response.data });
  //    })
  //    .catch((err) => {
  //       this.setState({ data: []});
  //    })
  // }

  handleSubmit = () => this.setState({ verified: true })

  render() {
    return (
      <div className="settings">
        <div className="email list">
          <label>User Email:</label>
          <span>User email</span><br/>
        </div>
        <div className="api_key list">
          <label>Api_key:</label>
          <span>API key should come from rest api because it should be same for particular user</span><br/>
        </div>
        <div className="verified list">
          <label>Verified:</label>
          <span>Not Verified</span><br/>
        </div>
        <p>Please click the verify button after integration we will check if integration process was done correctly</p>
        {this.state.verified ? <CircularProgress /> : <RaisedButton label="Verify" onClick={this.handleSubmit}/>}
        {this.props.location.state ? this.props.location.state.some : ''}
      </div>
    );
  }
}

export default Settings;
