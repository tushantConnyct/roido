import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import {Card} from 'material-ui/Card';

const style = {
  margin: 10,
};

const loginStyle = {
  padding: 10,
  textAlign: 'center',
};

const iconStyles = {
  marginRight: 24,
  float: 'right',
  marginTop: -15,
  fontSize: 35,
};

class Signin extends React.PureComponent {
  render() {
    return (
      <div className="login" style={loginStyle}>
        <Card>
          <FontIcon className="material-icons" style={iconStyles}>account_circle</FontIcon>
          <h4>Sign In Now</h4>
          <TextField
            type="email"
            hintText="Email"
          /><br />
          <TextField
            type="password"
            hintText="Password"
          /><br />
          <RaisedButton label="Submit" style={style} />
        </Card>
      </div>
    );
  }
}

export default Signin;
