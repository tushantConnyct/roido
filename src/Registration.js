import React from 'react';
import {browserHistory} from 'react-router';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import {Card} from 'material-ui/Card';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FacebookPlatform from './FacebookPlatform';
import FontIcon from 'material-ui/FontIcon';

class Registration extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      open: false,
      type: '',
      category: '',
      platform: '',
      categories: ["Food", "Travel", "Communication"],
      platforms: ["Facebook", "KIK", "Slack"],
      valueOfCategory: '',
      valueOfPlatform: '',
      botName: '',
      location: ''
    };

  }

  handleCategoryChange = (event, index, value) => this.setState({valueOfCategory: value});
  handlePlatformChange = (event, index, value) => this.setState({valueOfPlatform: value});
  handleBotName = (event) => this.setState({ botName: event.target.value });
  handleLocation = (event) => this.setState({ location: event.target.value });
  addCategoryChange = (event) => this.setState({ category: event.target.value });
  // addCategoryChange = (event) => this.setState({ categories: [...this.state.categories, event.target.value] });
  addPlatformChange = (event) => this.setState({ platform: event.target.value });
  // addPlatformChange = (event) => this.setState({ platform: [...this.state.platform, event.target.value] });
  // addPlatform = (event) => this.setState({ platform: value });

  handleOpen = (type) => this.setState({ open:true, type });
  handleClose = (event) => this.setState({ open:false });

  // addCategory = (event, type) => {
  //   event.preventDefault();
  //   this.setState({ open: false});
  // };

  addCategory = (event, type) => {
    event.preventDefault();
    console.log('type', type);
    console.log('event', event.which);
    type === 'category' ? this.setState({ categories: [...this.state.categories, this.state.category]}) :
     this.setState({ platforms: [...this.state.platforms, this.state.platform]});
     this.setState({open: false});
  };

  onSubmit = (event) => {
    event.preventDefault();
    const { botName, location, valueOfPlatform, valueOfCategory} = this.state;
    browserHistory.push({pathname: '/settings', state: { some: `${botName}` }});
    // const botName = this.state.botName;
    // const location = this.state.location;
    // const valueOfPlatform = this.state.botName;
    // const valueOfCategory = this.state.valueOfCategory;
    // if (!botName || !location || !valueOfCategory || !valueOfPlatform) {
    //
    // }
  }

  render() {
    const { botName, location, valueOfPlatform, valueOfCategory, type, platforms, categories} = this.state;
    console.log('platform', this.state.platform);
    const isEnabled = botName.length > 0 && location.length > 0 && valueOfPlatform > 0 && valueOfCategory > 0;
    console.log('isEnabled', isEnabled);
    return (
      <div className="registration">
        <Card>
          <TextField
            type="text"
            hintText="Bot name"
            onChange={this.handleBotName}
          /><br />
          <TextField
            type="text"
            hintText="Location"
            onChange={this.handleLocation}
          /><br />
          <Dialog
              title={`Add ${type}`}
              modal={false}
              open={this.state.open}
              onRequestClose={this.handleClose}
              >
              {type === 'category' ?
              <form onSubmit={(event) => this.addCategory(event, 'category')}>
              <TextField
                type="text"
                hintText="Category Name"
                onChange={this.addCategoryChange}
              />
            </form> :
            <form onSubmit={(event) => this.addCategory(event, 'platform')}>
              <TextField
                type="text"
                hintText="Platform Name"
                onChange={this.addPlatformChange}
              />
            </form>
            }
          </Dialog>
          <SelectField
            value={this.state.valueOfCategory}
            onChange={this.handleCategoryChange}
            hintText="Category"
          >
            {
              categories.map((item, index) => <MenuItem key={index} value={index+1} primaryText={item} />)
            }
          </SelectField>
          <FloatingActionButton onTouchTap={() => this.handleOpen('category')}><ContentAdd /></FloatingActionButton>
          <br/>
          <SelectField
            value={this.state.valueOfPlatform}
            onChange={this.handlePlatformChange}
            hintText="Platform"
          >
            {
              platforms.map((item, index) => <MenuItem value={index+1} primaryText={item} />)
            }
          </SelectField>
          <FloatingActionButton onTouchTap={() => this.handleOpen('platform')}><ContentAdd /></FloatingActionButton>
          <br/>
          {this.state.valueOfPlatform === 1 && <FacebookPlatform />}
          <RaisedButton label="Done" onClick={this.onSubmit} disabled={!isEnabled} />
        </Card>
      </div>
    );
  }
}

export default Registration;
