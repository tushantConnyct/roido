import React, { Component } from 'react';
import {Link} from 'react-router';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import {Card} from 'material-ui/Card';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import './App.css';

// const style = {
//   margin: 12,
// };
//
// const loginStyle = {
//   padding: 10,
//   textAlign: 'center',
// };
//
// const iconStyles = {
//   marginRight: 24,
//   float: 'right',
//   marginTop: -15,
//   fontSize: 35,
// };


class App extends Component {
  constructor(){
    super();
    this.state = { open: false };
  }

  handleToggle = () => this.setState({open: !this.state.open});


  render() {
    return (
      <div>
        <AppBar
          title="Dashboard"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
          onTouchTap={this.handleToggle}
        />
          <Drawer open={this.state.open} onRequestChange={this.handleToggle} docked={false}>
            <MenuItem containerElement={<Link to="/" />}>Dashboard</MenuItem>
            <MenuItem containerElement={<Link to="/settings" />}>Settings</MenuItem>
            <MenuItem containerElement={<Link to="/signin" />}>Signin</MenuItem>
            <MenuItem containerElement={<Link to="/registration" />}>Registration</MenuItem>
          </Drawer>
          <div style={{ textAlign: 'center' }}>
            {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;
