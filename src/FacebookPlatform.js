import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class FacebookPlatform  extends React.PureComponent {
  render() {
    return (
      <div className="facebook-platform">
        <TextField
          type="text"
          hintText="Bot name"
        /><br />
        <TextField
          type="text"
          hintText="Location"
        /><br />
        <TextField
          type="text"
          hintText="Description"
        /><br />
      </div>
    );
  }
}

export default FacebookPlatform;
