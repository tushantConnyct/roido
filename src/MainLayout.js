import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

class MainLayout extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <AppBar
          title="Dashboard"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
          onTouchTap={this.handleToggle}
        />
          <Drawer open={this.state.open}>
            <MenuItem to="/">Dashboard</MenuItem>
            <MenuItem to="settings">Settings</MenuItem>
          </Drawer>
          <div style={{ textAlign: 'center' }}>
            {this.props.children}
        </div>
      </div>
    );
  }
}

export default MainLayout;
