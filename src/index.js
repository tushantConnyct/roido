import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import Dashboard from './Dashboard';
import Settings from './Settings';
import Signin from './Signin';
import Registration from './Registration';
// import MainLayout from './MainLayout';

import App from './App';
import './index.css';

const Root = () => (
  <MuiThemeProvider>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
       <IndexRoute component={Dashboard} />
       <Route path="settings" component={Settings} />
       <Route path="signin" component={Signin} />
       <Route path="registration" component={Registration} />
     </Route>
    </Router>
  </MuiThemeProvider>
)

ReactDOM.render(<Root />, document.getElementById('root'));
